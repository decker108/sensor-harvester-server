CREATE ROLE grafana_user WITH PASSWORD 'secret' LOGIN;
GRANT CONNECT ON DATABASE autoclearskiesdb TO grafana_user;

CREATE ROLE autoclearskies_user WITH PASSWORD 'another-secret' LOGIN;
GRANT CONNECT ON DATABASE autoclearskiesdb TO autoclearskies_user;

CREATE TABLE misc_measurements(
id SERIAL PRIMARY KEY, 
lpg INT, 
lng INT, 
co INT, 
datetime TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW());

CREATE TABLE measurements(
id SERIAL PRIMARY KEY,
humidity INT,
radon_st_avg INT,
radon_lt_avg INT,
temperature INT,
pressure INT,
co2_level INT,
voc_level INT,
created TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW());

GRANT SELECT ON misc_measurements TO grafana_user;
GRANT SELECT ON measurements TO grafana_user;

GRANT SELECT,INSERT,UPDATE ON misc_measurements TO autoclearskies_user;
GRANT SELECT,USAGE,UPDATE ON measurements TO autoclearskies_user;
